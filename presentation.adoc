= Tutorial,  tips and tricks for working on the Linux command line​

----
... Our continuing mission: To seek out knowledge of C, to explore strange UNIX commands, and to boldly code where no one has man page 4. ​
----

== There are many shells, bash is only one of them.​

sh:: the standard shell: the "Bourne Shell".​
bash:: "Bourne Again Shell". Open Source (GPL) implementation of 'sh'.​
ksh:: Korn shell, POSIX standard, also based on 'sh'. Not recommended for interactive use IMHO.​
zsh:: Like bash and ksh, make 'sh' more palatable.​
csh:: should never be used for anything whatsoever, because it's full of bugs.​
tcsh:: make csh more palatable for interactive use and fixes 0.1% of its bugs. Should not be used at all either as far as I'm concerned.​

== Tools you should have in your toolbox​

man:: manual pages
tr:: Translate or delete characters
sed:: Stream Editor
awk:: pattern scanning and text processing language
grep:: pattern matching
xargs:: manipulate command lines
ps:: list running processes.
find:: find files and optionally perform actions on them.
test:: test for things, short cut is `+[ x = y ]+`. (`+[[ x = y  ]]+` is a bash enhancement)

== Common command switches
[source,shell]
----
tar xfz somefile.tar.gz # x = extract, f = filename, z = compress/uncompress.​
tar cvfz somefile.tar.gz somedir # c = create, v = verbose, etc...​

grep -i # i = ignore case ​
sed -i # i = substitute "in place"​

rm -i # i = interactive, it will ask you for confirmation​
cp –r # r = recursive copy​
rm –f # f = "use the force"​

find . -name '*.gz' -exec grep -i foo {} \; -print​
# name = the name / pattern that filenames need to match​
# exec = execute the command that follows on the found file.​
# {} = replace this with the found file.​
# \; escaped ; so the ; belongs to `grep -i foo filename` and not the `find` command.​
# print = print the filename​
----

​Often, they can be listed by typing `command --help`, `command -h` or `man command`

== Linking

* Symbolic links, aka soft linkgs

[source,shell]
----
ln -s presentation.adoc foo
ls -li # print the inode numbers
total 4
48251376 lrwxrwxrwx 1 neutvd afd_rdwd   16 Sep 27 08:39 foo -> presentation.adoc
48251168 -rw-r--r-- 1 neutvd afd_rdwd 2356 Sep 27 08:39 presentation.adoc
----

This is just a pointer to a file, editing the pointer and can go accross file systems, but removing `+presentation.adoc` will cause a dangling link:
[source,shell]
----
echo "Real content" > the-real-file
ln -s the-real-file pointer-to-real-file
rm the-real-file
ls -li
lrwxrwxrwx 1 neutvd afd_rdwd   13 Sep 27 09:25 pointer-to-real-file -> the-real-file
-rw-r--r-- 2 neutvd afd_rdwd 2830 Sep 27 09:25 presenttion.adoc
cat pointer-to-real-file 
cat: pointer-to-real-file: No such file or directory
----

* Hard links

[source,shell]
----
ln presenttion.adoc foo
ls -li
total 8
48251168 -rw-r--r-- 2 neutvd afd_rdwd 2431 Sep 27 08:40 foo
48251168 -rw-r--r-- 2 neutvd afd_rdwd 2431 Sep 27 08:40 presenttion.adoc
----

The files are the same, editing one edits the other, but deleting one, keeps the other around.
[source,shell]
----
ln the-real-file the-other-real-file
ls -li
total 12
48251168 -rw-r--r-- 1 neutvd afd_rdwd 3063 Sep 27 09:28 presentation.adoc
48251370 -rw-r--r-- 2 neutvd afd_rdwd   13 Sep 27 09:31 the-other-real-file
48251370 -rw-r--r-- 2 neutvd afd_rdwd   13 Sep 27 09:31 the-real-file
rm the-real-file 
cat the-other-real-file 
Real content
----
== Process control

 `+&+` operator:: start process in the background​
 `+jobs+`:: show jobs running in current shell.​
 `+kill %n+`:: kill a job by its job number​
 `+kill nnn+`:: kill a process by its PID (=Process ID)​

== Redirection and pipes​

Pipe:: use the output of a process as the input of another:​
[source,shell]
----
ps | grep foo # print process with name 'foo'.​
----
Store output of a process into a file:​
[source,shell]
----
ps > processes.txt​
----
Copy a file without using `cp`:​
[source,shell]
----
cat < infile > outfile​
----
Store error output to a different file from normal output:​
[source,shell]
----
docker run myimage > output.txt 2> errors.txt​
----
The number is a so-called "file descriptor" 0 = stdin, 1 = stdout, 2 = stderr. But you can remap them (man 2 dup):​
[source,shell]
----
docker run myimage > output.txt 2>&1 # both stdout and stderr to output.txt
----

== Setting environment variables​

When to use 'export':​

Only visible by the shell:​
[source,shell]
----
MYVAR="hello world"​
----
Visible by all the shell's child processes:​
[source,shell]
----
MYVAR="hello world"​
export MYVAR​
----
Or:​
[source,shell]
----
export MYVAR="hello world"​
----

== Setting environment variable's value from command output​

Back ticks:​
[source,shell]
----
myvar=`ls`​
----
New and improved way:​
[source,shell]
----
myvar=$(ls)​
----
Doing it from a file:​
[source,shell]
----
. somefile.env # "dot operator"​
----

== Conditionals and loops​

[source,shell]
----
if $foo = "bar" ; then​
  echo "Hello world!"​
fi​
----

[source,shell]
----
for line in $(cat somefile.txt) ; do​
  echo $line​
done-
----

[source,shell]
----
​while /bin/true ; do​
  echo asdfasdf​
done​
----

== Data types

Bash has arrays and associative arrays (you will probably know them as "dictionaries" or "hash tables")​

[source,shell]
----
declare -A animals​
animals["moo"]=cow​
animals["woof"]=dog​
echo "${animals[@]}"​
dog cow​
echo "${animals[moo]}"​
cow​
----

== Features for interactive use

Ctrl-r:: (Cmd-r on mac?) search backwards in command line history​
Ctrl-_:: Undo command line edit.​
Alt-#::  Comment out current line and press enter. Effectively stores command in history without executing it.​
Alt-.:: Insert last argument of previous command, repeat to go further back in time.​
Ctrl-u:: Delete from cursor to the beginning of the line. Also works for password fields in most GUI applications also.

And many, many more, but the above are very useful and I use them all the time.

== Terminal multiplexers

tmux:: recommended because actively being developed.​
screen:: afaik development stopped.​

Allows you to attach and detach a terminal session, which is useful if you have a long running process in a terminal on a remote machine and you need to suspend your laptop to catch a bus.​
